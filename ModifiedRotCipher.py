class ModifiedRotCipher():

    RELATIVE_FREQUENCIES = {'A' : 0.08167, 'B' : 0.01492, 'C' : 0.02782,
                            'D' : 0.04253, 'E' : 0.12702, 'F' : 0.02228,
                            'G' : 0.02015, 'H' : 0.06094, 'I' : 0.06966,
                            'J' : 0.00153, 'K' : 0.00772, 'L' : 0.04025,
                            'M' : 0.02406, 'N' : 0.06749, 'O' : 0.07507,
                            'P' : 0.01929, 'Q' : 0.00095, 'R' : 0.05987,
                            'S' : 0.06327, 'T' : 0.09056, 'U' : 0.02758,
                            'V' : 0.00978, 'W' : 0.02360, 'X' : 0.00150,
                            'Y' : 0.01974, 'Z' : 0.00074}

    def encrypt(self, text, key, shift):

        alphabet = self.__get_alphabet(key, shift)

        cipher_text = ""

        # Loop over each character of the text
        for char in text:

            # Don't bother encrypting if it's a space
            if char == ' ':
                cipher_text += char
                continue

            offset = ord(char) - ord('A')

            # Need to select letters from the alphabet
            cipher_text += alphabet[offset % 26]

        return cipher_text


    def decrypt(self, text, shift):

        return self.encrypt(text, 26 - shift)


    def crack(self, text):

        # Need to load word list, loop and pass them in as keys to
        # __calculate_frequency_difference
        frequencies = [self.__calculate_frequency_difference(shift) for shift in range(1, 25)]

        return self.decrypt(text, 1 + frequencies.index(min(frequencies)))



    def __calculate_frequency_difference(self, text, shift):
        """Calculates the difference between this frequency and the expected frequency"""

        total_letters = len(text)

        plain_text = self.decrypt(text, shift)

        freqs = {}

        for char in plain_text.lower():

            if not char == ' ':
                if char in freqs:
                    freqs[char] += 1 / total_letters
                else:
                    freqs[char] = 1


        # Calculate the sum of all individual differences
        return sum(list(map(lambda letter: abs(self.RELATIVE_FREQUENCIES[letter] - freqs[letter]), freqs)))


    def __get_alphabet(self, key, shift):
        """Gets the alphabet with key inserted at beginning that is used for encryption"""

        alphabet = key + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        new_alphabet = ""

        seen_before = set()

        for char in alphabet:
            if char not in seen_before:
                new_alphabet += char

                seen_before.add(char)


        # Shift the alphabet
        new_alphabet = new_alphabet[shift::] + new_alphabet[:shift]

        return new_alphabet
    