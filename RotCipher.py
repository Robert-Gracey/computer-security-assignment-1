class RotCipher():

    RELATIVE_FREQUENCIES = {'a' : 0.08167, 'b' : 0.01492, 'c' : 0.02782,
                            'd' : 0.04253, 'e' : 0.12702, 'f' : 0.02228,
                            'g' : 0.02015, 'h' : 0.06094, 'i' : 0.06966,
                            'j' : 0.00153, 'k' : 0.00772, 'l' : 0.04025,
                            'm' : 0.02406, 'n' : 0.06749, 'o' : 0.07507,
                            'p' : 0.01929, 'q' : 0.00095, 'r' : 0.05987,
                            's' : 0.06327, 't' : 0.09056, 'u' : 0.02758,
                            'v' : 0.00978, 'w' : 0.02360, 'x' : 0.00150,
                            'y' : 0.01974, 'z' : 0.00074}


    def encrypt(self, text, shift):

        cipher_text = ""

        # Loop over each character of the text
        for char in text:

            # Don't bother encrypting if it's a space
            if char == ' ':
                cipher_text += char
                continue
            
            # Need to calculate the offset so we encrypt uppercase and
            # lowercase letter correctly
            offset = ord('A') if char.isupper() else ord('a')

            # Actually encrypt the letter
            cipher_text += chr(offset + ((ord(char) - offset) + shift) % 26)

        return cipher_text


    def decrypt(self, text, shift):
        # Decrypting is the same as encrypting
        return self.encrypt(text, 26 - shift)


    def crack(self, text):
        
        # Calculate the the frequency differences for each possible shift
        frequencies = [self.__calculate_frequency_difference(text, shift) for shift in range(1, 25)]

        # Shift amount is 1 + the lowest frequency difference index
        shift_amount = 1 + frequencies.index(min(frequencies))

        # Decrypt the text with the cracked shift_amount
        decrypted_text = self.decrypt(text, shift_amount)

        return (shift_amount, decrypted_text)



    def __calculate_frequency_difference(self, text, shift):
        """Calculates the difference between this frequency and the expected frequency"""

        # Find the total number of letter within the text
        total_letters = len(text)

        # Decrypt the text with this shift amount
        plain_text = self.decrypt(text, shift)

        freqs = {}

        # Loop over each character in the decrypted text and calculate
        # its frequency
        for char in plain_text.lower():

            if not char == ' ':
                if char in freqs:
                    freqs[char] += 1 / total_letters
                else:
                    freqs[char] = 1

        # Calculate the sum of all individual frequency differences
        return sum(list(map(lambda letter: abs(self.RELATIVE_FREQUENCIES[letter] - freqs[letter]), freqs)))
    